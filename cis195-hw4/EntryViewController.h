//
//  EntryViewController.h
//  cis195-hw4
//
//  Created by Martin Greenberg on 11/1/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JournalEntry.h"

@interface EntryViewController : UIViewController
@property (strong, nonatomic) JournalEntry *entry;
@end
