//
//  EntryViewController.m
//  cis195-hw4
//
//  Created by Martin Greenberg on 11/1/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import "EntryViewController.h"
#import "JournalEntry.h"
#import <CoreLocation/CoreLocation.h>

@interface EntryViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UITextView *descriptionView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation EntryViewController
@synthesize entry;

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	self.title = entry.title;
	double latitude = ((CLLocation*)entry.location).coordinate.latitude;
	double longitude = ((CLLocation*)entry.location).coordinate.longitude;
	[self.infoLabel setText:[NSString stringWithFormat:@"Location: %f, %f",latitude, longitude]];
	self.descriptionView.text = entry.entryDescription;
	[self.image setImage:entry.image];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
