//
//  JournalEntry.h
//  cis195-hw4
//
//  Created by Martin Greenberg on 11/3/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface JournalEntry : NSManagedObject

@property (nonatomic, retain) NSDate * creationDate;
@property (nonatomic, retain) id entryAnnotation;
@property (nonatomic, retain) NSString * entryDescription;
@property (nonatomic, retain) id image;
@property (nonatomic, retain) id location;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * distance;

@end
