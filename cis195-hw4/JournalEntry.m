//
//  JournalEntry.m
//  cis195-hw4
//
//  Created by Martin Greenberg on 11/3/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import "JournalEntry.h"


@implementation JournalEntry

@dynamic creationDate;
@dynamic entryAnnotation;
@dynamic entryDescription;
@dynamic image;
@dynamic location;
@dynamic title;
@dynamic distance;

@end
