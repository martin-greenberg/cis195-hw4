//
//  JournalListViewController.h
//  cis195-hw4
//
//  Created by Martin Greenberg on 11/1/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

@interface JournalListViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic,strong) NSFetchedResultsController *controller;
@end
