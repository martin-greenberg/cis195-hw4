//
//  JournalListViewController.m
//  cis195-hw4
//
//  Created by Martin Greenberg on 11/1/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JournalListViewController.h"
#import "JournalEntry.h"
#import "MainViewController.h"
#import "EntryViewController.h"
#import "NewEntryViewController.h"

@interface JournalListViewController ()
@end

@implementation JournalListViewController
@synthesize managedObjectContext;
@synthesize controller;
NSFetchRequest *requestByDate;
UIBarButtonItem *sortDateButton;
UIBarButtonItem *sortDistanceButton;
UIBarButtonItem *addButton;
NSEntityDescription *entity;
NSFetchRequest *requestByDate;
NSFetchRequest *requestByDistance;
CLLocationManager *locationManager;
CLLocation *userLocation;
UIActivityIndicatorView *indicator;
int locationDone = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
	addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addItem:)];
	sortDateButton = [[UIBarButtonItem alloc] initWithTitle:@"Sort by Date" style:UIBarButtonItemStylePlain target:self action:@selector(sortDate:)];
	sortDistanceButton = [[UIBarButtonItem alloc] initWithTitle:@"Sort by Distance" style:UIBarButtonItemStylePlain target:self action:@selector(sortDistance:)];
	
	self.navigationItem.rightBarButtonItems = @[sortDistanceButton, addButton];
	MainViewController * rootView = (MainViewController *) self.tabBarController;
	self.managedObjectContext = rootView.mos;
	entity = [NSEntityDescription entityForName:@"JournalEntry" inManagedObjectContext:self.managedObjectContext];
	requestByDate = [[NSFetchRequest alloc] initWithEntityName:entity.name];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"creationDate" ascending:NO];
	[requestByDate setSortDescriptors:@[sortDescriptor]];
	controller = [[NSFetchedResultsController alloc]
												initWithFetchRequest:requestByDate
												managedObjectContext:self.managedObjectContext
												sectionNameKeyPath:nil
												cacheName:@"dateResults"];
	NSError *error;
	BOOL success = [controller performFetch:&error];
	if(!success) NSLog(@"Could not fetch results.\n");
}

- (void) sortDistance:(id) sender{
	if(self.controller.fetchedObjects.count > 0){
	locationManager = [[CLLocationManager alloc] init];
	[locationManager setDelegate:self];
	[locationManager requestWhenInUseAuthorization];
	if ([CLLocationManager locationServicesEnabled]) {
		if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
			[locationManager startUpdatingLocation];
		}
	}
	indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:indicator], addButton];
	[self.view setNeedsDisplay];
	[indicator startAnimating];
	}
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	userLocation = [locations lastObject];
	[locationManager stopUpdatingLocation];
	[indicator stopAnimating];
	self.navigationItem.rightBarButtonItems = @[sortDateButton, addButton];
	for(JournalEntry * entry in self.controller.fetchedObjects){
		double distanceToEntry = [entry.location distanceFromLocation:userLocation];
		entry.distance = [NSNumber numberWithDouble:distanceToEntry];
	}
	requestByDistance = [[NSFetchRequest alloc] initWithEntityName:entity.name];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"distance" ascending:YES];
	[requestByDistance setSortDescriptors:@[sortDescriptor]];
	controller = [[NSFetchedResultsController alloc]
				  initWithFetchRequest:requestByDistance
				  managedObjectContext:self.managedObjectContext
				  sectionNameKeyPath:nil
				  cacheName:@"distanceResults"];
	NSError *error;
	BOOL success = [controller performFetch:&error];
	if(!success) NSLog(@"Could not fetch results.\n");
	[self.tableView beginUpdates];
	[self.tableView deleteSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
	[self.tableView reloadData];
	[self.tableView insertSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
	[self.tableView endUpdates];
	[self.view setNeedsDisplay];
}

-(void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	userLocation = nil;
	[locationManager stopUpdatingLocation];
	[indicator stopAnimating];
	self.navigationItem.rightBarButtonItems = @[sortDistanceButton, addButton];
	[self.view setNeedsDisplay];
}

- (void) sortDate: (id) sender{
	self.navigationItem.rightBarButtonItems = @[sortDistanceButton, addButton];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"creationDate" ascending:NO];
	[requestByDate setSortDescriptors:@[sortDescriptor]];
	controller = [[NSFetchedResultsController alloc]
				  initWithFetchRequest:requestByDate
				  managedObjectContext:self.managedObjectContext
				  sectionNameKeyPath:nil
				  cacheName:@"dateResults"];
	NSError *error;
	BOOL success = [controller performFetch:&error];
	if(!success) NSLog(@"Could not fetch results.\n");
	[self.tableView beginUpdates];
	[self.tableView deleteSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
	[self.tableView reloadData];
	[self.tableView insertSections:[[NSIndexSet alloc] initWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
	[self.tableView endUpdates];
	

}

- (void) viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	NSError *error;
	BOOL success = [controller performFetch:&error];
	if(!success) NSLog(@"Could not fetch results.\n");
	[self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return (NSInteger) controller.fetchedObjects.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"journalEntry"];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"journalEntry"];
	}
	JournalEntry *entry = [controller objectAtIndexPath:indexPath];
	if(entry != nil){
		cell.textLabel.text = entry.title;
		cell.imageView.image = entry.image;
		cell.detailTextLabel.text = entry.entryDescription;
	}
	cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self performSegueWithIdentifier: @"viewEntry" sender: [controller.fetchedObjects objectAtIndex:indexPath.row]];
}
 
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	[self performSegueWithIdentifier:@"editEntry" sender:[controller.fetchedObjects objectAtIndex:indexPath.row]];
}

- (void)addItem:sender {
	[self performSegueWithIdentifier:@"newEntry" sender:nil];
}

-(IBAction)unwindToList:(UIStoryboardSegue *) segue
{
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([segue.identifier isEqualToString:@"viewEntry"]){
		EntryViewController* dest = (EntryViewController*) segue.destinationViewController;
		dest.entry = sender;
	} else {
		NewEntryViewController* dest = (NewEntryViewController*) segue.destinationViewController;
		dest.entry = sender;
		dest.prevWasMap = 0;
	}
}

@end
