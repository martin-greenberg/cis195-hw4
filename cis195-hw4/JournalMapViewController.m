//
//  JournalMapViewController.m
//  cis195-hw4
//
//  Created by Martin Greenberg on 11/1/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JournalMapViewController.h"
#import "MainViewController.h"
#import "JournalEntry.h"
#import "EntryViewController.h"
#import "NewEntryViewController.h"
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

@interface JournalMapViewController()

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic,strong) NSFetchedResultsController *controller;
@end

@implementation JournalMapViewController
NSMutableDictionary* entryMap;
NSManagedObjectContext *context;
CLLocationCoordinate2D userCoord;
UILongPressGestureRecognizer *longPressRecognizer;
CLLocationCoordinate2D mapCoordForNewEntry;

-(void) setMapView:(MKMapView *)mapView
{
	_mapView = mapView;
	self.mapView.mapType = MKMapTypeStandard;
	self.mapView.delegate = self;
}

-(void) viewDidLoad {
	[super viewDidLoad];
	entryMap = [[NSMutableDictionary alloc] init];
	MainViewController * rootView = (MainViewController *) self.tabBarController;
	context = rootView.mos;
	UIBarButtonItem* addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addItem:)];
	UIBarButtonItem* findUserButton = [[UIBarButtonItem alloc] initWithTitle:@"Find Me" style:UIBarButtonItemStylePlain target:self action:@selector(centerOnUser:)];

	self.navigationItem.rightBarButtonItems = @[findUserButton, addButton];
	NSEntityDescription *entity = [NSEntityDescription
								   entityForName:@"JournalEntry" inManagedObjectContext:context];
	
	NSFetchRequest *requestByDate = [[NSFetchRequest alloc] init];
	[requestByDate setEntity:entity];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
										initWithKey:@"creationDate" ascending:NO];
	[requestByDate setSortDescriptors:@[sortDescriptor]];
	self.controller = [[NSFetchedResultsController alloc]
				  initWithFetchRequest:requestByDate
				  managedObjectContext:context
				  sectionNameKeyPath:nil
				  cacheName:@"dateResults"];
	
	self.locationManager = [[CLLocationManager alloc] init];
	self.locationManager.delegate = self;
	[self.locationManager requestWhenInUseAuthorization];
	if ([CLLocationManager locationServicesEnabled]) {
		if ([CLLocationManager authorizationStatus] !=kCLAuthorizationStatusDenied) {
			self.mapView.showsUserLocation = YES;
		}
	}
}

-(void) addEntryByLongPress:(UIGestureRecognizer*) sender{
	if(sender.state == UIGestureRecognizerStateEnded){
		[self.mapView removeGestureRecognizer:longPressRecognizer];
	} else {
		CGPoint mapPoint = [sender locationInView:self.mapView];
		mapCoordForNewEntry = [self.mapView convertPoint:mapPoint toCoordinateFromView:self.mapView];
		[self performSegueWithIdentifier:@"addEntryFromMap" sender:nil];
	}
}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	mapCoordForNewEntry = CLLocationCoordinate2DMake(0, 0);
	longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(addEntryByLongPress:)];
	[self.mapView addGestureRecognizer:longPressRecognizer];
	NSError *error;
	BOOL success = [self.controller performFetch:&error];
	if(!success) NSLog(@"Could not fetch results.\n");
	for(JournalEntry * mapEntry in self.controller.fetchedObjects){
		if(mapEntry.location != nil){
			CLLocation *entryLoc = ((CLLocation*)mapEntry.location);
			MKPointAnnotation *pointAnnotation = [[MKPointAnnotation alloc] init];
			pointAnnotation.title = mapEntry.title;
			pointAnnotation.subtitle = mapEntry.entryDescription;
			pointAnnotation.coordinate = entryLoc.coordinate;
			[self.mapView addAnnotation:pointAnnotation];
			NSString* entryKey = [[NSString alloc] initWithString:pointAnnotation.description];
			[entryMap setObject:mapEntry forKey:entryKey];
		}
	}
}

-(IBAction)centerOnUser:(id)sender
{
	MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userCoord, 500, 500);
	[self.mapView setRegion:region animated:YES];
}

-(IBAction)showEntryDetails:(id)sender
{
	[self performSegueWithIdentifier:@"viewEntryFromMap" sender:[entryMap objectForKey:((MKPointAnnotation*)sender).description]];
}

-(IBAction)addItem:(id)sender
{
	[self performSegueWithIdentifier:@"addEntryFromMap" sender:[entryMap objectForKey:((MKPointAnnotation*)sender).description]];
}

- (MKAnnotationView *)mapView:(MKMapView *)sender viewForAnnotation:(id<MKAnnotation>)annotation {
	MKAnnotationView *aView = [sender dequeueReusableAnnotationViewWithIdentifier:@"entryMapID"];
	if (!aView) {
		JournalEntry* entry = [entryMap objectForKey:annotation.description];
		CLLocation* entryLoc = entry.location;
		CLLocationCoordinate2D entryCoord = entryLoc.coordinate;
		if((entry != nil) && ((entryCoord.latitude != 0) || (entryCoord.longitude != 0))){
			aView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"entryMapID"];
			aView.canShowCallout = YES;
			UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
			aView.rightCalloutAccessoryView = rightButton;
			UIImageView *preview = [[UIImageView alloc] initWithImage:((JournalEntry *)[entryMap objectForKey:annotation.description]).image];
			[preview setBounds:CGRectMake(0, 0, 50, 50)];
			aView.leftCalloutAccessoryView = preview;
		} else {
			return nil;
		}
	}
	aView.annotation = annotation;
	return aView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
	JournalEntry* toView = [entryMap objectForKey:view.annotation.description];
	[self performSegueWithIdentifier:@"viewEntryFromMap" sender:toView];
}

-(IBAction)unwindToMap:(UIStoryboardSegue *) segue
{
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
	CLLocationCoordinate2D coordinate = userLocation.coordinate;
	userCoord = coordinate;
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([segue.identifier isEqualToString:@"viewEntryFromMap"]){
		EntryViewController* dest = (EntryViewController*) segue.destinationViewController;
		dest.entry = sender;
	} else {
		NewEntryViewController* dest = (NewEntryViewController*) segue.destinationViewController;
		dest.entry = sender;
		if((mapCoordForNewEntry.latitude != 0) || (mapCoordForNewEntry.longitude != 0)){
			dest.mapCoord = mapCoordForNewEntry;
		} else {
			dest.mapCoord = CLLocationCoordinate2DMake(0, 0);
		}
		dest.prevWasMap = 1;
	}
}
@end