//
//  NewEntryViewController.h
//  cis195-hw4
//
//  Created by Martin Greenberg on 11/1/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "JournalEntry.h"
#import <CoreLocation/CoreLocation.h>

@import CoreLocation;
@interface NewEntryViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, CLLocationManagerDelegate, UITextViewDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic) JournalEntry *entry;
@property (nonatomic) int prevWasMap;
@property (nonatomic) CLLocationCoordinate2D mapCoord; //the coordinate if the segue was prompted by a long press on map
@end
