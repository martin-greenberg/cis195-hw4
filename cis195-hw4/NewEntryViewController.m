//
//  NewEntryViewController.m
//  cis195-hw4
//
//  Created by Martin Greenberg on 11/1/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "NewEntryViewController.h"
#import "MainViewController.h"
#import <CoreData/CoreData.h>
@interface NewEntryViewController ()
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UITextView *descriptionField;
@property (strong, nonatomic) IBOutlet UIImageView *imageSelected;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingLocation;
@property (strong, nonatomic) IBOutlet UILabel *loadingLocationLabel;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *imageSelectButton;
@property (strong, nonatomic) IBOutlet UITextField *titleField;

@end

@implementation NewEntryViewController
UIImagePickerController *imagePicker;
CLLocationManager *locationManager;
CLLocation *location;
NSManagedObjectContext *context;

- (void)viewDidLoad {
    [super viewDidLoad];
	MainViewController * rootView = (MainViewController *) self.tabBarController;
	context = rootView.mos;
	}

-(void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if((self.entry != nil) || (self.prevWasMap && ((self.mapCoord.latitude != 0) || (self.mapCoord.longitude != 0)))){
		if(self.entry != nil){
			self.title = @"Edit Entry";
			location = self.entry.location;
			self.titleField.text = self.entry.title;
			self.descriptionField.text = self.entry.entryDescription;
			if(self.entry.location == nil){
				self.locationLabel.text = @"No location recorded.";
			} else {
				double latitude = ((CLLocation*)self.entry.location).coordinate.latitude;
				double longitude = ((CLLocation*)self.entry.location).coordinate.longitude;
				self.locationLabel.text = [NSString stringWithFormat:@"Location: %f, %f",latitude, longitude];
			}
		} else {
			self.title = @"Create New Entry";
			location = [[CLLocation alloc] initWithCoordinate:self.mapCoord altitude:0 horizontalAccuracy:0 verticalAccuracy:0 course:0 speed:0 timestamp:[[NSDate alloc] init]];
			double latitude = self.mapCoord.latitude;
			double longitude = self.mapCoord.longitude;
			self.locationLabel.text = [NSString stringWithFormat:@"Location: %f, %f",latitude, longitude];
		}
		self.loadingLocation.hidden = YES;
		self.loadingLocationLabel.hidden = YES;
		self.locationLabel.hidden = NO;
		
		if(self.entry.image != nil){
			self.imageSelectButton.hidden = YES;
			self.imageSelected.image = self.entry.image;
		}
	} else {
		self.title = @"Create New Entry";
		locationManager = [[CLLocationManager alloc] init];
		[locationManager setDelegate:self];
		[locationManager requestWhenInUseAuthorization];
		if ([CLLocationManager locationServicesEnabled]) {
			if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
				self.loadingLocation.hidden = NO;
				[self.loadingLocation startAnimating];
				self.loadingLocationLabel.hidden = NO;
				[locationManager startUpdatingLocation];
			}
		}
	}
	
	imagePicker = [[UIImagePickerController alloc] init];
	imagePicker.delegate = self;
	imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
	self.imageSelected.userInteractionEnabled = YES;
}

	

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	[self.loadingLocation stopAnimating];
	self.loadingLocation.hidden = YES;
	self.loadingLocationLabel.hidden = YES;
	location = [locations lastObject];
	double latitude = [location coordinate].latitude;
	double longitude = [location coordinate].longitude;
	[self.locationLabel setText:[NSString stringWithFormat:@"Location: %f, %f",latitude, longitude]];
	self.locationLabel.hidden = NO;
	[locationManager stopUpdatingLocation];
}

-(void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	[self.loadingLocation stopAnimating];
	self.loadingLocation.hidden = YES;
	self.loadingLocationLabel.hidden = YES;
	[self.locationLabel setText:@"No location found."];
	self.locationLabel.hidden = NO;
	location = nil;
	[locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	
	[self dismissViewControllerAnimated:picker completion:nil];
	self.imageSelectButton.hidden = YES;
	self.imageSelected.image = [info valueForKey:UIImagePickerControllerOriginalImage];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[self dismissViewControllerAnimated:picker completion:nil];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	if ([textView.text isEqualToString:@"Description (optional)"]) {
		textView.text = @"";
		textView.textColor = [UIColor blackColor];
	}
	[textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
	if ([textView.text isEqualToString:@""]) {
		textView.text = @"Description (optional)";
		textView.textColor = [UIColor lightGrayColor];
	}
	[textView resignFirstResponder];
}

- (IBAction)hideKeyboard:(id)sender
{
	[sender resignFirstResponder];
}

- (IBAction)saveAction:(id)sender
{
	if([self.titleField.text isEqualToString:@""]){
		UIAlertController* saveAlert = [UIAlertController alertControllerWithTitle:@"No title entered"
													message:@"You need to enter a title for the entry." preferredStyle:UIAlertControllerStyleAlert];
		UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:nil];
		[saveAlert addAction:okAction];
		[self presentViewController:saveAlert animated:YES completion:nil];
	} else {
		if(self.entry == nil){
			self.entry = (JournalEntry *)[NSEntityDescription insertNewObjectForEntityForName:@"JournalEntry" inManagedObjectContext:context];
			self.entry.location = location;
			self.entry.creationDate = [[NSDate alloc] init];
		}
		self.entry.title = self.titleField.text;
		if(![self.descriptionField.text isEqualToString:@"Description (optional)"]) {
			self.entry.entryDescription = self.descriptionField.text;
		} else {
			self.entry.entryDescription = @"";
		}
		self.entry.image = self.imageSelected.image;
		NSError *error;
		if([context save:&error] == NO){//save failed
			UIAlertController* saveFailure = [UIAlertController alertControllerWithTitle:@"Save failed"
																			   message:@"Could not save new entry." preferredStyle:UIAlertControllerStyleAlert];
			UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:nil];
			[saveFailure addAction:okAction];
			[self presentViewController:saveFailure animated:YES completion:nil];
		}
		if(self.prevWasMap){
			[self performSegueWithIdentifier:@"mapUnwind" sender:self];
		} else {
			[self performSegueWithIdentifier:@"listUnwind" sender:self];
		}
	}
}

- (IBAction)pickPhoto:(UITapGestureRecognizer *) gestureRecognizer
{
	UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Select source:"
																   message:@"Where would you like to select an image from?" preferredStyle:UIAlertControllerStyleActionSheet];
	UIAlertAction* cameraAction = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault
														 handler:^(UIAlertAction * action) {
															 imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
															 [self presentViewController:imagePicker animated:YES completion:nil];
															}];
	UIAlertAction* cameraRollAction = [UIAlertAction actionWithTitle:@"Camera Roll" style:UIAlertActionStyleDefault
														 handler:^(UIAlertAction * action) { imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
															 [self presentViewController:imagePicker animated:YES completion:nil];
															}];
	UIAlertAction* photoLibraryAction = [UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault
														 handler:^(UIAlertAction * action) { imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
															[self presentViewController:imagePicker animated:YES completion:nil];
														 }];
	UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
														 handler:^(UIAlertAction * action) {}];
	
	if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) [alert addAction:cameraAction];
	if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) [alert addAction:cameraRollAction];
	if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) [alert addAction:photoLibraryAction];
	[alert addAction:cancelAction];
	[self presentViewController:alert animated:YES completion:nil];
}
@end
